<?php
        include("inc/header.php");
        include("inc/sidenav.php");
?>
   
      <section id="content">
        
       
        

        <!--start container-->
        <div class="container">
          <div class="section">
  <!--Hoverable Table-->
            <div class="divider"></div>
            <div id="hoverable-table">
              <h4 class="header">User's</h4>
              <div class="row">
               
                <div class="col s12">
                  <table class="hoverable">
                    <thead>
                      <tr>
                        <th data-field="id">username</th>
                        <th data-field="name">Nick Name</th>
                        <th data-field="price">Actions</th>
                      </tr>
                    </thead>
                    <tbody>
					<?php 
					$res=$conn2->query("select * from user");
					if ($res->num_rows > 0) {
	 while($row = $res->fetch_assoc()) {
		 ?>
		  <tr>
                        <td><a href="log.php?username[]=<?php echo$row['username']; ?>"><?php echo $row['username']; ?></a></td>
                        <td><?php echo $row['Name']; ?></td>
                        <td><a href="user-action.php?edit=1&username=<?php echo$row['username']; ?>"><i class="mdi-editor-mode-edit"></i></a>-<a href="delete-action.php?username=<?php echo$row['username']; ?>"><i class="mdi-action-delete"></i></a></td>
                      </tr>
		 <?php
	 }
					}
					?>
                    </tbody>
                  </table>
                </div>
              </div>
            </div>

        </div>
        <!--end container-->

      </section>
      <!-- END CONTENT -->

      <!-- //////////////////////////////////////////////////////////////////////////// -->
     

    </div>
    <!-- END WRAPPER -->

  </div>
  <!-- END MAIN -->


<?php
include("inc/footer.php");
?>