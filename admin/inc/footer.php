<br /><br /><br />
<footer class="page-footer" style=" position:fixed;bottom:0px; height:40px; width:100%;">
       
        <div class="footer-copyright">
            <div class="container">
                Copyright � 2016 All rights reserved.
                <span class="right"> Design and Developed by <a class="grey-text text-lighten-4" href="http://saran.cu.cc/">Saran</a></span>
            </div>
        </div>
    </footer>
    <script type="text/javascript" src="js/plugins/jquery-1.11.2.min.js"></script>    
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <script type="text/javascript" src="js/plugins/perfect-scrollbar/perfect-scrollbar.min.js"></script>
	 <script type="text/javascript" src="js/plugins/prism/prism.js"></script>
    <script type="text/javascript" src="js/plugins/chartist-js/chartist.min.js"></script>   
    <script type="text/javascript" src="js/plugins/chartjs/chart.min.js"></script>
    <script type="text/javascript" src="js/plugins/chartjs/chart-script.js"></script>
    <script type="text/javascript" src="js/plugins/sparkline/jquery.sparkline.min.js"></script>
    <script type="text/javascript" src="js/plugins/sparkline/sparkline-script.js"></script>
   
    <script type="text/javascript" src="js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script type="text/javascript" src="js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <script type="text/javascript" src="js/plugins/jvectormap/vectormap-script.js"></script>
    
    <script type="text/javascript" src="js/plugins.min.js"></script>
    <script type="text/javascript" src="js/custom-script.js"></script>
   <script>
  
  var $input =  $('.tdt').pickadate();

var picker = $input.pickadate('picker');
//picker.set('select', new Date(<?php echo ($_GET['to']);?>));
var pickers = $('.fdt').pickadate().pickadate('picker');
//pickers.set('select', new Date(2015, 3, 06));
        
   </script>
</body>
</html>