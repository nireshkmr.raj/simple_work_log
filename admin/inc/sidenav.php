 <?php 
 function navhlp($name,$thenm)
 {
	 //if(str_replace(".php", "", basename($_SERVER["SCRIPT_NAME"]))=="index")
	 if(str_replace(".php", "", basename($name))==$thenm)
	 { 
 echo 'active';
 }
 }
 ?>
    <div id="main">
        <!-- START WRAPPER -->
        <div class="wrapper">

            <!-- START LEFT SIDEBAR NAV-->
            <aside id="left-sidebar-nav">
                <ul id="slide-out" class="side-nav fixed leftside-navigation">
                <li class="user-details cyan darken-2">
                <div class="row">
                    <div class="col col s4 m4 l4">
                        <img src="images/avatar.jpg" alt="" class="circle responsive-img valign profile-image">
                    </div>
                    <div class="col col s8 m8 l8">
                        <ul id="profile-dropdown" class="dropdown-content">
                            <li><a href="user-action.php?username=<?php echo $_SESSION['dyna-uname'];?>"><i class="mdi-action-face-unlock"></i> Profile</a>
                            </li>
                            <li><a href="logout.php"><i class="mdi-hardware-keyboard-tab"></i> Logout</a>
                            </li>
                        </ul>
                        <a class="btn-flat dropdown-button waves-effect waves-light white-text profile-btn" href="#" data-activates="profile-dropdown"><?php echo $_SESSION['dyna-user'];?><i class="mdi-navigation-arrow-drop-down right"></i></a>
                        <p class="user-roal"><?php 
						if($_SESSION['dyna-extra']=="1")
						{
							echo 'Administrator';
						}else{
							echo 'User';
						}
						?></p>
                    </div>
                </div>
                </li>
				
                <li class="<?php navhlp($_SERVER["SCRIPT_NAME"],"index"); ?> bold"><a href="index.php" class="waves-effect waves-cyan"><i class="mdi-action-dashboard"></i> Dashboard</a>
                </li>
               
				<li class="no-padding">
                    <ul class="collapsible collapsible-accordion">
                        <li class="bold"><a  class="<?php navhlp($_SERVER["SCRIPT_NAME"],"users"); ?> <?php navhlp($_SERVER["SCRIPT_NAME"],"edit-user"); ?> <?php navhlp($_SERVER["SCRIPT_NAME"],"user-action"); ?> collapsible-header waves-effect waves-cyan"><i class="mdi-action-account-circle"></i>Users </a>
                            <div class="collapsible-body">
                                <ul>
                                    <li class="<?php navhlp($_SERVER["SCRIPT_NAME"],"users"); ?> bold"><a href="users.php" class="">All Users</a>
                                    </li>
                                    <li><a href="user-action.php">Add Users</a>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </li>
                <li class="<?php navhlp($_SERVER["SCRIPT_NAME"],"log"); ?> bold"><a href="log.php" class="waves-effect waves-cyan"><i class="mdi-editor-insert-chart"></i> Logs</a>
                </li>

            </ul>
                <a href="#" data-activates="slide-out" class="sidebar-collapse btn-floating btn-medium waves-effect waves-light hide-on-large-only cyan"><i class="mdi-navigation-menu"></i></a>
            </aside>
            <!-- END LEFT SIDEBAR NAV-->
