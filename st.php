<?php 
include("h.php");
?>
<!DOCTYPE html>
<html class="stopwatch-mode">
<head lang="en">
    <meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1">
	<meta name="mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<title>Timer for amazon alexa</title>
    <link type="text/css" rel="stylesheet" href="assets/materialize/css/materialize.min.css">
    <link type="text/css" rel="stylesheet" href="assets/css/styles.css">
</head>
<body>

<div class="wrapper">



	<div class="app">

		

		<div class="container stopwatch">
			<form>
				<a id="stopwatch-btn-start" class="waves-effect waves-teal btn-flat">Start</a>
				<a id="stopwatch-btn-pause" class="waves-effect waves-teal btn-flat">Pause</a>
				<a id="stopwatch-btn-reset" class="waves-effect waves-teal btn-flat">Reset</a>
			</form>

			<div class="clock inactive z-depth-1">
				<span id="tm">0:00:00.0</span>
				<div class="overlay waves-effect"></div>
			</div>
			<a class="waves-effect waves-teal btn-flat" id="go_on">Submit log</a>
		</div>

		<div class="container timer ">
			<form>
				
			</form>

		</div>

<div class="container alarm">
</div>
	</div>

	

</div>

<script src="/j.js"></script>
<script src="assets/materialize/js/materialize.min.js"></script>
<script src="assets/jonthornton-jquery-timepicker/jquery.timepicker.min.js"></script>
<script src="assets/js/hammer.js"></script>
<script src="assets/js/script.js"></script>
<script src="assets/js/alarm.js"></script>
<script src="assets/js/stopwatch.js"></script>
<script src="assets/js/timer.js"></script>
<script>
$('#go_on').click(function(){
	var url = 's.php';
var form = $('<form action="' + url + '" method="post">' +
  '<input type="hidden" name="tm" value="' + $("#tm").html() + '" />' +
  '</form>');
$('body').append(form);
form.submit();
})
</script>
</body>
</html>
