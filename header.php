<!DOCTYPE html>
<html lang="en">
 <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1">
     <meta name="description" content=""> <meta name="author" content="">
      <title>Simple Work Log</title>
       <link href="css/bootstrap.min.css" rel="stylesheet">
       <link href="css/bootstrap-theme.min.css" rel="stylesheet">
        <link href="css/ie10-viewport-bug-workaround.css" rel="stylesheet">
         <link href="dashboard.css" rel="stylesheet">
         <script src="js/ie-emulation-modes-warning.js"></script>
         <!--[if lt IE 9]>
         <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js">
       </script>
       <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js">
     </script><![endif]-->
   </head>
   <body>
      <nav class="navbar navbar-inverse navbar-fixed-top">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
             <a class="navbar-brand" href="index.php">Simple Work Log</a>
           </div>
           <div id="navbar" class="navbar-collapse collapse">
             <ul class="nav navbar-nav navbar-right">
                <ul class="nav navbar-nav">
                  <li class="dropdown">
                    <?php
                     if(isset($login)){
                       ?>
                       <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                         <?php echo $_COOKIE["u_id"]; ?>
                         <span class="caret"></span>
                       </a>
                       <ul class="dropdown-menu">
                         <li><a href="st.php">Start Work</a></li>
                         <li role="separator" class="divider"></li>
                         <li><a href="index.php?logout">logout</a></li>
                       </ul>
                       <?php  } ?>
                     </li>
                   </ul>
                  </ul>
                </div>
              </div>
            </nav>
